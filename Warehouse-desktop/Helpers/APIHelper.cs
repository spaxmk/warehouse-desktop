﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Pipes;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Warehouse_desktop.Models;
using Warehouse_desktop.Views;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace Warehouse_desktop.Helpers
{
    public partial class APIHelper
    {
        private static string baseUrl = "http://spaxmk-001-site1.ftempurl.com";
        public static HttpClient apiClient;
        public static AuthenticatedUser loggedUser;

        //Login on server, pickup user
        public static async Task<AuthenticatedUser> Authenticate(string username, string password)
        {
            var inputData = new Dictionary<string, string>
            {
                {"grant_type", "password" },
                {"username", username },
                {"password", password }
            };
            var data = new FormUrlEncodedContent(inputData);


            apiClient = new HttpClient();
            apiClient.BaseAddress = new Uri(baseUrl);
            apiClient.DefaultRequestHeaders.Accept.Clear();
            apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            using (HttpResponseMessage response = await apiClient.PostAsync("/Token", data))
            {
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsAsync<AuthenticatedUser>();
                    loggedUser = new AuthenticatedUser(result.Access_Token, result.UserName);
                    return result;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        //Get all warehouses from server
        public static async Task<List<Warehouse>> GetAll()
        {
            var request = new RestRequest(Method.GET);
            var client = new RestClient(baseUrl + "/api/Warehouses/");
            client.Timeout = -1;
            request.AddHeader("Authorization", "Bearer " + loggedUser.Access_Token);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            if(response.StatusCode == HttpStatusCode.Unauthorized)
            {
                MessageBox.Show("Not authorized GET Method for " + loggedUser.UserName);
            }
            var listData = JsonConvert.DeserializeObject<List<Warehouse>>(response.Content);
            return listData;

        }

        //GetAllAssigments

        public static async Task<List<Assigment>> GetAllAssigments()
        {
            var request = new RestRequest(Method.GET);
            var client = new RestClient(baseUrl + "/api/Assigment/");
            client.Timeout = -1;
            request.AddHeader("Authorization", "Bearer " + loggedUser.Access_Token);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            var listData = JsonConvert.DeserializeObject<List<Assigment>>(response.Content);
            return listData;

        }

        public static async Task<string> PostAssugments(string Id, string Name, string Description, string Completed, string AccountEmail)
        {
            var inputData = new Dictionary<string, string>
            {
                //{ "Id", id },
                { "Name",Name },
                { "Description",Description },
                { "Completed", Completed },
                { "AccountEmail", AccountEmail }
            };

            var input = new FormUrlEncodedContent(inputData);
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + loggedUser.Access_Token);
                using (HttpResponseMessage res = await client.PostAsync(baseUrl + "/api/Assigment/", input))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;
        }

        //Update Assigment
        public static async Task<string> PutAssigments(string Id, string Name, string Description, string Completed, string AccountEmail)
        {
            var inputData = new Dictionary<string, string>
            {
                { "Id", Id },
                { "Name",Name },
                { "Description",Description },
                { "Completed", Completed },
                { "AccountEmail", AccountEmail }
            };

            var input = new FormUrlEncodedContent(inputData);
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + loggedUser.Access_Token);
                using (HttpResponseMessage res = await client.PutAsync(baseUrl + "/api/Assigment/" + Id, input))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        //if (data != null)
                        //{
                        //    return data;
                        //}
                        if (res.IsSuccessStatusCode)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;
        }

        //Delete assigment
        public static async Task<string> DeleteAssigment(string id)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + loggedUser.Access_Token);
                using (HttpResponseMessage res = await client.DeleteAsync(baseUrl + "/api/Assigment/" + id))
                {
                    using (HttpContent content = res.Content)
                    {
                        MessageBox.Show("Deleted: " + ((int)res.StatusCode).ToString() + " - " + res.StatusCode.ToString());
                        string data = await content.ReadAsStringAsync();
                        if (res.IsSuccessStatusCode)
                        {
                            return data;
                        }
                    }
                }
            }
            return string.Empty;
        }

        //Register new Account
        //POST api/Account/Register
        public static async Task<string> CreateNewAccount(string email, string password, string confirmPassword, string firstName, string lastName, string phoneNumber, string adress, string city)
        {
            var inputData = new Dictionary<string, string>
            {
                { "Email", email },
                {"Password", password },
                {"ConfirmPassword", confirmPassword },
                {"FirstName", firstName },
                {"LastName", lastName },
                {"PhoneNumber", phoneNumber },
                {"Adress", adress },
                {"City", city }
            };
            var input = new FormUrlEncodedContent(inputData);

            using(HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.PostAsync(baseUrl + "/api/Account/Register/", input))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (res.IsSuccessStatusCode && data != null)
                        {
                            return data;
                            MessageBox.Show("You have successfully registered a new user " + firstName + " " + lastName + ".");
                        }
                        else
                        {
                            MessageBox.Show( res.IsSuccessStatusCode.ToString() + "\n Invalid data ");
                        }
                    }
                }
            }
            return string.Empty;

        }

        //api/Assigment?email={email}
        public static async Task<List<Assigment>> GetAllAssigmentsByEmail( string accountEmail)
        {
            string _accountEmail = accountEmail;
            
            var request = new RestRequest(Method.GET);
            var client = new RestClient(baseUrl + "/api/Assigment?email=" + _accountEmail);
            client.Timeout = -1;
            request.AddHeader("Authorization", "Bearer " + loggedUser.Access_Token);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            var listData = JsonConvert.DeserializeObject<List<Assigment>>(response.Content);
            return listData;

        }

        public static async Task<List<AllUsersViewModel>> GetAllUsers()
        {
            var client = new RestClient (baseUrl + "/api/Account/GetAllUser");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + loggedUser.Access_Token);
            request.AddParameter("text/plain", "", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            var listData = JsonConvert.DeserializeObject<List<AllUsersViewModel>>(response.Content);
            return listData;

        }



    }
}
