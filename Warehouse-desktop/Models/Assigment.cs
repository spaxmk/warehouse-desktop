﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse_desktop.Models
{
    public class Assigment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Completed { get; set; }
        public string AccountEmail { get; set; }
    }
}
