﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse_desktop.Models
{
    public class Warehouse
    {
        public int Id { get; set; }
        public string Adress { get; set; }
        public string City { get; set; }
        public string Contact { get; set; }
        public string Name { get; set; }
        public double Surface { get; set; }

    }
}
