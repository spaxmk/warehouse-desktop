﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.OData.Edm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse_desktop.Models
{
    public class AuthenticatedUser
    {
        public string Access_Token { get; set; }
        public string UserName { get; set; }

        public AuthenticatedUser(string Access_Token, string UserName)
        {
            this.Access_Token = Access_Token;
            this.UserName = UserName;
        }

    }

    public class AllUsersViewModel
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Adress { get; set; }
        public string City { get; set; }
        public ICollection<IdentityUserRole> Roles { get; set; }

    }
}
