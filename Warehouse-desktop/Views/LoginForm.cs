﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Deployment.Internal;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Warehouse_desktop.Helpers;
using Warehouse_desktop.Views;

namespace Warehouse_desktop
{
    public partial class LoginForm : Form
    {
        private string _userName { get; set; }
        private string _password { get; set; }

        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
            }
        }

        public LoginForm()
        {
            InitializeComponent();
            Timer timerCheckConn = new Timer();
            timerCheckConn.Tick += new EventHandler(timerCheckConn_Tick);
            timerCheckConn.Start();
            //HomeForm hf = new HomeForm();
            //hf.Show();
        }

        public bool CanLogin
        {
            get
            {
                bool output = false;
                if(UserName?.Length > 0 && Password?.Length > 0)
                {
                    output = true;
                }
                return output;
            }
        }

        public async Task Login(string username, string password)
        {
            var result = await APIHelper.Authenticate(username, password);
            if(result.UserName != null)
            {
                //lblFaildLogin.Text = "Uspesno ulogovani";
                this.Hide();
                HomeForm homeForm = new HomeForm();
                homeForm.Show();

            }
            else
            {
                lblFaildLogin.ForeColor = Color.Red;
                lblFaildLogin.Text = "BadReques";
            }
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            lblFaildLogin.ForeColor = Color.Green;
            lblFaildLogin.Text = "Please wait ...";

            _userName = txtUsername.Text.ToString();
            _password = txtPassword.Text.ToString();

            if (CanLogin)
            {
                try
                {
                    await Login(_userName, _password);
                }
                catch (Exception)
                {
                    lblFaildLogin.ForeColor = Color.Red;
                    lblFaildLogin.Text = "Username or Password is incorrect";
                }
            }
            else
            {
                lblFaildLogin.ForeColor = Color.Red;
                lblFaildLogin.Text = "Username and password required";
            }

        }

        private void txtUsername_Leave(object sender, EventArgs e)
        {
            if (txtUsername.Text == "")
            {
                lblUsernameRequired.ForeColor = Color.Red;
                lblUsernameRequired.Text = "Username is a required field!";
            }
            else
            {
                lblUsernameRequired.Text = "";
            }
        }

        private void txtPassword_Leave(object sender, EventArgs e)
        {
            if (txtPassword.Text == "")
            {
                lblPasswordRequired.ForeColor = Color.Red;
                lblPasswordRequired.Text = "Password is a required field!";
            }
            else
            {
                lblPasswordRequired.Text = "";
            }
        }

        private void timerCheckConn_Tick(object sender, EventArgs e)
        {
            CheckConn();
        }

        public void CheckConn()
        {
            bool status = false;
            try
            {
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() != true)
                {
                    if(btnLogin.Enabled != false)
                    {
                        btnLogin.Enabled = false;
                    }
                    lblCheckConn.ForeColor = Color.Red;
                    lblCheckConn.Text = "Connection network problem.";
                }
                else
                {
                    if(btnLogin.Enabled != true)
                    {
                        btnLogin.Enabled = true;
                    }
                    lblCheckConn.Text = "";
                }
            }
            catch (Exception  e)
            {
                Console.WriteLine(e.ToString());
                throw;
            }
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            txtPassword.PasswordChar = '*';
        }
    }
}
