﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Warehouse_desktop.Helpers;
using Warehouse_desktop.Models;

namespace Warehouse_desktop.Views
{
    public partial class WarehouseForm : Form
    {
        public bool statusConnection = false;

        public WarehouseForm()
        {
            InitializeComponent();
            Timer timerCheckConn = new Timer();
            timerCheckConn.Tick += new EventHandler(timerCheckConn_Tick);
            timerCheckConn.Start();
            if (statusConnection == true)
            {
                MessageBox.Show("Connection problem.");
            }
            else
            {
                LoadDataAsync();
            }
        }

        public async Task LoadDataAsync()
        {
            dataGridView1.Update();
            var dataList = await APIHelper.GetAll();
            //var dataList = await GetAll();
            dataGridView1.DataSource = dataList;
        }

        private async void btnRefresh_Click(object sender, EventArgs e)
        {
            await LoadDataAsync();
        }

        private void timerCheckConn_Tick(object sender, EventArgs e)
        {
            CheckConn();
        }

        public void CheckConn()
        {
            try
            {
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() != true)
                {
                    if (btnRefresh.Enabled != false)
                    {
                        btnRefresh.Enabled = false;
                        statusConnection = false;
                    }
                }
                else
                {
                    if (btnRefresh.Enabled != true)
                    {
                        btnRefresh.Enabled = true;
                        statusConnection = true;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw;
            }
        }
    }
}
