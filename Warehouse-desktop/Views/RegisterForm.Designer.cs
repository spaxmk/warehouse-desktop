﻿namespace Warehouse_desktop.Views
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelRegister = new System.Windows.Forms.Panel();
            this.lblCityRequired = new System.Windows.Forms.Label();
            this.lblAdresRequired = new System.Windows.Forms.Label();
            this.lblPhoneNumberRequired = new System.Windows.Forms.Label();
            this.lblLastNameRequired = new System.Windows.Forms.Label();
            this.lblFirstNameRequired = new System.Windows.Forms.Label();
            this.lblConPrassRequired = new System.Windows.Forms.Label();
            this.lblPassRequired = new System.Windows.Forms.Label();
            this.lblEmailRequired = new System.Windows.Forms.Label();
            this.btnRegister = new System.Windows.Forms.Button();
            this.lblRegister = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lvlPassword = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblConfirmPassword = new System.Windows.Forms.Label();
            this.txtAdress = new System.Windows.Forms.TextBox();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.txtPhoneNumber = new System.Windows.Forms.TextBox();
            this.lblLastName = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.lblPhoneNumber = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.lblAdress = new System.Windows.Forms.Label();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.lvlCity = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.panelQRCode = new System.Windows.Forms.Panel();
            this.lblScanQRCode = new System.Windows.Forms.Label();
            this.pbQRCode = new System.Windows.Forms.PictureBox();
            this.lblFaildReg = new System.Windows.Forms.Label();
            this.RegiserFormElipse = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.panelRegister.SuspendLayout();
            this.panelQRCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbQRCode)).BeginInit();
            this.SuspendLayout();
            // 
            // panelRegister
            // 
            this.panelRegister.Controls.Add(this.lblCityRequired);
            this.panelRegister.Controls.Add(this.lblAdresRequired);
            this.panelRegister.Controls.Add(this.lblFaildReg);
            this.panelRegister.Controls.Add(this.lblPhoneNumberRequired);
            this.panelRegister.Controls.Add(this.lblLastNameRequired);
            this.panelRegister.Controls.Add(this.lblFirstNameRequired);
            this.panelRegister.Controls.Add(this.lblConPrassRequired);
            this.panelRegister.Controls.Add(this.lblPassRequired);
            this.panelRegister.Controls.Add(this.lblEmailRequired);
            this.panelRegister.Controls.Add(this.btnRegister);
            this.panelRegister.Controls.Add(this.lblRegister);
            this.panelRegister.Controls.Add(this.lblEmail);
            this.panelRegister.Controls.Add(this.lvlPassword);
            this.panelRegister.Controls.Add(this.txtCity);
            this.panelRegister.Controls.Add(this.lblConfirmPassword);
            this.panelRegister.Controls.Add(this.txtAdress);
            this.panelRegister.Controls.Add(this.lblFirstName);
            this.panelRegister.Controls.Add(this.txtPhoneNumber);
            this.panelRegister.Controls.Add(this.lblLastName);
            this.panelRegister.Controls.Add(this.txtLastName);
            this.panelRegister.Controls.Add(this.lblPhoneNumber);
            this.panelRegister.Controls.Add(this.txtFirstName);
            this.panelRegister.Controls.Add(this.lblAdress);
            this.panelRegister.Controls.Add(this.txtConfirmPassword);
            this.panelRegister.Controls.Add(this.lvlCity);
            this.panelRegister.Controls.Add(this.txtPassword);
            this.panelRegister.Controls.Add(this.txtEmail);
            this.panelRegister.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelRegister.Location = new System.Drawing.Point(0, 0);
            this.panelRegister.Name = "panelRegister";
            this.panelRegister.Size = new System.Drawing.Size(340, 481);
            this.panelRegister.TabIndex = 0;
            // 
            // lblCityRequired
            // 
            this.lblCityRequired.AutoSize = true;
            this.lblCityRequired.ForeColor = System.Drawing.Color.Red;
            this.lblCityRequired.Location = new System.Drawing.Point(116, 393);
            this.lblCityRequired.Name = "lblCityRequired";
            this.lblCityRequired.Size = new System.Drawing.Size(0, 13);
            this.lblCityRequired.TabIndex = 25;
            // 
            // lblAdresRequired
            // 
            this.lblAdresRequired.AutoSize = true;
            this.lblAdresRequired.ForeColor = System.Drawing.Color.Red;
            this.lblAdresRequired.Location = new System.Drawing.Point(116, 348);
            this.lblAdresRequired.Name = "lblAdresRequired";
            this.lblAdresRequired.Size = new System.Drawing.Size(0, 13);
            this.lblAdresRequired.TabIndex = 24;
            // 
            // lblPhoneNumberRequired
            // 
            this.lblPhoneNumberRequired.AutoSize = true;
            this.lblPhoneNumberRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPhoneNumberRequired.Location = new System.Drawing.Point(116, 303);
            this.lblPhoneNumberRequired.Name = "lblPhoneNumberRequired";
            this.lblPhoneNumberRequired.Size = new System.Drawing.Size(0, 13);
            this.lblPhoneNumberRequired.TabIndex = 23;
            // 
            // lblLastNameRequired
            // 
            this.lblLastNameRequired.AutoSize = true;
            this.lblLastNameRequired.ForeColor = System.Drawing.Color.Red;
            this.lblLastNameRequired.Location = new System.Drawing.Point(116, 256);
            this.lblLastNameRequired.Name = "lblLastNameRequired";
            this.lblLastNameRequired.Size = new System.Drawing.Size(0, 13);
            this.lblLastNameRequired.TabIndex = 22;
            // 
            // lblFirstNameRequired
            // 
            this.lblFirstNameRequired.AutoSize = true;
            this.lblFirstNameRequired.ForeColor = System.Drawing.Color.Red;
            this.lblFirstNameRequired.Location = new System.Drawing.Point(116, 217);
            this.lblFirstNameRequired.Name = "lblFirstNameRequired";
            this.lblFirstNameRequired.Size = new System.Drawing.Size(0, 13);
            this.lblFirstNameRequired.TabIndex = 21;
            // 
            // lblConPrassRequired
            // 
            this.lblConPrassRequired.AutoSize = true;
            this.lblConPrassRequired.ForeColor = System.Drawing.Color.Red;
            this.lblConPrassRequired.Location = new System.Drawing.Point(116, 168);
            this.lblConPrassRequired.Name = "lblConPrassRequired";
            this.lblConPrassRequired.Size = new System.Drawing.Size(0, 13);
            this.lblConPrassRequired.TabIndex = 20;
            // 
            // lblPassRequired
            // 
            this.lblPassRequired.AutoSize = true;
            this.lblPassRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPassRequired.Location = new System.Drawing.Point(116, 128);
            this.lblPassRequired.Name = "lblPassRequired";
            this.lblPassRequired.Size = new System.Drawing.Size(0, 13);
            this.lblPassRequired.TabIndex = 19;
            // 
            // lblEmailRequired
            // 
            this.lblEmailRequired.AutoSize = true;
            this.lblEmailRequired.ForeColor = System.Drawing.Color.Red;
            this.lblEmailRequired.Location = new System.Drawing.Point(116, 86);
            this.lblEmailRequired.Name = "lblEmailRequired";
            this.lblEmailRequired.Size = new System.Drawing.Size(0, 13);
            this.lblEmailRequired.TabIndex = 18;
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(212, 407);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(115, 34);
            this.btnRegister.TabIndex = 16;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // lblRegister
            // 
            this.lblRegister.AutoSize = true;
            this.lblRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegister.Location = new System.Drawing.Point(82, 28);
            this.lblRegister.Name = "lblRegister";
            this.lblRegister.Size = new System.Drawing.Size(151, 18);
            this.lblRegister.TabIndex = 17;
            this.lblRegister.Text = "Register new account";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(20, 68);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(32, 13);
            this.lblEmail.TabIndex = 0;
            this.lblEmail.Text = "Email";
            // 
            // lvlPassword
            // 
            this.lvlPassword.AutoSize = true;
            this.lvlPassword.Location = new System.Drawing.Point(20, 108);
            this.lvlPassword.Name = "lvlPassword";
            this.lvlPassword.Size = new System.Drawing.Size(53, 13);
            this.lvlPassword.TabIndex = 1;
            this.lvlPassword.Text = "Password";
            this.lvlPassword.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(117, 370);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(169, 20);
            this.txtCity.TabIndex = 15;
            // 
            // lblConfirmPassword
            // 
            this.lblConfirmPassword.AutoSize = true;
            this.lblConfirmPassword.Location = new System.Drawing.Point(20, 148);
            this.lblConfirmPassword.Name = "lblConfirmPassword";
            this.lblConfirmPassword.Size = new System.Drawing.Size(90, 13);
            this.lblConfirmPassword.TabIndex = 2;
            this.lblConfirmPassword.Text = "Confirm password";
            // 
            // txtAdress
            // 
            this.txtAdress.Location = new System.Drawing.Point(117, 325);
            this.txtAdress.Name = "txtAdress";
            this.txtAdress.Size = new System.Drawing.Size(169, 20);
            this.txtAdress.TabIndex = 14;
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(20, 193);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(55, 13);
            this.lblFirstName.TabIndex = 3;
            this.lblFirstName.Text = "First name";
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Location = new System.Drawing.Point(117, 280);
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(169, 20);
            this.txtPhoneNumber.TabIndex = 13;
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(20, 238);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(56, 13);
            this.lblLastName.TabIndex = 4;
            this.lblLastName.Text = "Last name";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(117, 233);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(169, 20);
            this.txtLastName.TabIndex = 12;
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.AutoSize = true;
            this.lblPhoneNumber.Location = new System.Drawing.Point(20, 283);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(76, 13);
            this.lblPhoneNumber.TabIndex = 5;
            this.lblPhoneNumber.Text = "Phone number";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(117, 190);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(169, 20);
            this.txtFirstName.TabIndex = 11;
            // 
            // lblAdress
            // 
            this.lblAdress.AutoSize = true;
            this.lblAdress.Location = new System.Drawing.Point(20, 328);
            this.lblAdress.Name = "lblAdress";
            this.lblAdress.Size = new System.Drawing.Size(39, 13);
            this.lblAdress.TabIndex = 6;
            this.lblAdress.Text = "Adress";
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.Location = new System.Drawing.Point(117, 145);
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.Size = new System.Drawing.Size(169, 20);
            this.txtConfirmPassword.TabIndex = 10;
            this.txtConfirmPassword.TextChanged += new System.EventHandler(this.txtConfirmPassword_TextChanged);
            // 
            // lvlCity
            // 
            this.lvlCity.AutoSize = true;
            this.lvlCity.Location = new System.Drawing.Point(20, 373);
            this.lvlCity.Name = "lvlCity";
            this.lvlCity.Size = new System.Drawing.Size(24, 13);
            this.lvlCity.TabIndex = 7;
            this.lvlCity.Text = "City";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(117, 105);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(169, 20);
            this.txtPassword.TabIndex = 9;
            this.txtPassword.TextChanged += new System.EventHandler(this.txtPassword_TextChanged_1);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(117, 65);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(169, 20);
            this.txtEmail.TabIndex = 8;
            // 
            // panelQRCode
            // 
            this.panelQRCode.AutoSize = true;
            this.panelQRCode.Controls.Add(this.lblScanQRCode);
            this.panelQRCode.Controls.Add(this.pbQRCode);
            this.panelQRCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQRCode.Location = new System.Drawing.Point(340, 0);
            this.panelQRCode.Name = "panelQRCode";
            this.panelQRCode.Size = new System.Drawing.Size(382, 481);
            this.panelQRCode.TabIndex = 1;
            // 
            // lblScanQRCode
            // 
            this.lblScanQRCode.AutoSize = true;
            this.lblScanQRCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScanQRCode.Location = new System.Drawing.Point(109, 334);
            this.lblScanQRCode.Name = "lblScanQRCode";
            this.lblScanQRCode.Size = new System.Drawing.Size(0, 18);
            this.lblScanQRCode.TabIndex = 20;
            // 
            // pbQRCode
            // 
            this.pbQRCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbQRCode.Image = global::Warehouse_desktop.Properties.Resources.Warehouse_icon;
            this.pbQRCode.Location = new System.Drawing.Point(83, 121);
            this.pbQRCode.Name = "pbQRCode";
            this.pbQRCode.Size = new System.Drawing.Size(210, 210);
            this.pbQRCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbQRCode.TabIndex = 19;
            this.pbQRCode.TabStop = false;
            // 
            // lblFaildReg
            // 
            this.lblFaildReg.AutoSize = true;
            this.lblFaildReg.Location = new System.Drawing.Point(20, 451);
            this.lblFaildReg.Name = "lblFaildReg";
            this.lblFaildReg.Size = new System.Drawing.Size(0, 13);
            this.lblFaildReg.TabIndex = 18;
            // 
            // RegiserFormElipse
            // 
            this.RegiserFormElipse.BorderRadius = 26;
            this.RegiserFormElipse.TargetControl = this;
            // 
            // RegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 481);
            this.Controls.Add(this.panelQRCode);
            this.Controls.Add(this.panelRegister);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RegisterForm";
            this.Text = "Register";
            this.panelRegister.ResumeLayout(false);
            this.panelRegister.PerformLayout();
            this.panelQRCode.ResumeLayout(false);
            this.panelQRCode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbQRCode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelRegister;
        private System.Windows.Forms.Panel panelQRCode;
        private System.Windows.Forms.Label lvlCity;
        private System.Windows.Forms.Label lblAdress;
        private System.Windows.Forms.Label lblPhoneNumber;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblConfirmPassword;
        private System.Windows.Forms.Label lvlPassword;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblRegister;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox txtAdress;
        private System.Windows.Forms.TextBox txtPhoneNumber;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblFaildReg;
        private Guna.UI2.WinForms.Guna2Elipse RegiserFormElipse;
        private System.Windows.Forms.PictureBox pbQRCode;
        private System.Windows.Forms.Label lblCityRequired;
        private System.Windows.Forms.Label lblAdresRequired;
        private System.Windows.Forms.Label lblPhoneNumberRequired;
        private System.Windows.Forms.Label lblLastNameRequired;
        private System.Windows.Forms.Label lblFirstNameRequired;
        private System.Windows.Forms.Label lblConPrassRequired;
        private System.Windows.Forms.Label lblPassRequired;
        private System.Windows.Forms.Label lblEmailRequired;
        private System.Windows.Forms.Label lblScanQRCode;
    }
}