﻿namespace Warehouse_desktop.Views
{
    partial class AssigmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlFrame = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtFilter = new Guna.UI2.WinForms.Guna2TextBox();
            this.panelDataGrid = new System.Windows.Forms.Panel();
            this.dataAssigments = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.completedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accountEmailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assigmentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panelOptions = new System.Windows.Forms.Panel();
            this.lblStatusCode = new System.Windows.Forms.Label();
            this.panelAddNew = new System.Windows.Forms.Panel();
            this.groupAdd = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.allUsersViewModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.txtCompleted = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.btnUcitajPodatke = new System.Windows.Forms.Button();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.guna2Elipse2 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.guna2Elipse3 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.timerCheckConn = new System.Windows.Forms.Timer(this.components);
            this.pnlFrame.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelDataGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataAssigments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.assigmentBindingSource)).BeginInit();
            this.panelOptions.SuspendLayout();
            this.panelAddNew.SuspendLayout();
            this.groupAdd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allUsersViewModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlFrame
            // 
            this.pnlFrame.Controls.Add(this.panel1);
            this.pnlFrame.Controls.Add(this.panelDataGrid);
            this.pnlFrame.Controls.Add(this.panelOptions);
            this.pnlFrame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFrame.Location = new System.Drawing.Point(0, 0);
            this.pnlFrame.Name = "pnlFrame";
            this.pnlFrame.Size = new System.Drawing.Size(808, 580);
            this.pnlFrame.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtFilter);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(200, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(608, 66);
            this.panel1.TabIndex = 5;
            // 
            // txtFilter
            // 
            this.txtFilter.BorderRadius = 20;
            this.txtFilter.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtFilter.DefaultText = "";
            this.txtFilter.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txtFilter.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txtFilter.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txtFilter.DisabledState.Parent = this.txtFilter;
            this.txtFilter.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txtFilter.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.txtFilter.FocusedState.Parent = this.txtFilter;
            this.txtFilter.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.txtFilter.ForeColor = System.Drawing.Color.Black;
            this.txtFilter.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.txtFilter.HoverState.Parent = this.txtFilter;
            this.txtFilter.IconLeft = global::Warehouse_desktop.Properties.Resources.search_26px;
            this.txtFilter.IconLeftOffset = new System.Drawing.Point(5, 0);
            this.txtFilter.Location = new System.Drawing.Point(13, 14);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.PasswordChar = '\0';
            this.txtFilter.PlaceholderText = "Search by Email:";
            this.txtFilter.SelectedText = "";
            this.txtFilter.ShadowDecoration.Parent = this.txtFilter;
            this.txtFilter.Size = new System.Drawing.Size(227, 36);
            this.txtFilter.TabIndex = 1;
            this.txtFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFilter_KeyDown);
            // 
            // panelDataGrid
            // 
            this.panelDataGrid.AutoSize = true;
            this.panelDataGrid.Controls.Add(this.dataAssigments);
            this.panelDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDataGrid.Location = new System.Drawing.Point(200, 0);
            this.panelDataGrid.Name = "panelDataGrid";
            this.panelDataGrid.Size = new System.Drawing.Size(608, 580);
            this.panelDataGrid.TabIndex = 1;
            // 
            // dataAssigments
            // 
            this.dataAssigments.AllowUserToAddRows = false;
            this.dataAssigments.AllowUserToDeleteRows = false;
            this.dataAssigments.AutoGenerateColumns = false;
            this.dataAssigments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataAssigments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn,
            this.completedDataGridViewTextBoxColumn,
            this.accountEmailDataGridViewTextBoxColumn});
            this.dataAssigments.DataSource = this.assigmentBindingSource;
            this.dataAssigments.Location = new System.Drawing.Point(11, 72);
            this.dataAssigments.Name = "dataAssigments";
            this.dataAssigments.ReadOnly = true;
            this.dataAssigments.Size = new System.Drawing.Size(590, 496);
            this.dataAssigments.TabIndex = 4;
            this.dataAssigments.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataAssigments_CellClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Width = 41;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.Width = 60;
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            this.descriptionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // completedDataGridViewTextBoxColumn
            // 
            this.completedDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.completedDataGridViewTextBoxColumn.DataPropertyName = "Completed";
            this.completedDataGridViewTextBoxColumn.HeaderText = "Completed";
            this.completedDataGridViewTextBoxColumn.Name = "completedDataGridViewTextBoxColumn";
            this.completedDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // accountEmailDataGridViewTextBoxColumn
            // 
            this.accountEmailDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.accountEmailDataGridViewTextBoxColumn.DataPropertyName = "AccountEmail";
            this.accountEmailDataGridViewTextBoxColumn.HeaderText = "Account Email";
            this.accountEmailDataGridViewTextBoxColumn.Name = "accountEmailDataGridViewTextBoxColumn";
            this.accountEmailDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // assigmentBindingSource
            // 
            this.assigmentBindingSource.DataSource = typeof(Warehouse_desktop.Models.Assigment);
            // 
            // panelOptions
            // 
            this.panelOptions.Controls.Add(this.lblStatusCode);
            this.panelOptions.Controls.Add(this.panelAddNew);
            this.panelOptions.Controls.Add(this.btnUcitajPodatke);
            this.panelOptions.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelOptions.Location = new System.Drawing.Point(0, 0);
            this.panelOptions.Name = "panelOptions";
            this.panelOptions.Size = new System.Drawing.Size(200, 580);
            this.panelOptions.TabIndex = 0;
            // 
            // lblStatusCode
            // 
            this.lblStatusCode.AutoSize = true;
            this.lblStatusCode.Location = new System.Drawing.Point(12, 383);
            this.lblStatusCode.Name = "lblStatusCode";
            this.lblStatusCode.Size = new System.Drawing.Size(62, 13);
            this.lblStatusCode.TabIndex = 7;
            this.lblStatusCode.Text = "StatusCode";
            // 
            // panelAddNew
            // 
            this.panelAddNew.Controls.Add(this.groupAdd);
            this.panelAddNew.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAddNew.Location = new System.Drawing.Point(0, 0);
            this.panelAddNew.Name = "panelAddNew";
            this.panelAddNew.Size = new System.Drawing.Size(200, 380);
            this.panelAddNew.TabIndex = 6;
            // 
            // groupAdd
            // 
            this.groupAdd.Controls.Add(this.btnDelete);
            this.groupAdd.Controls.Add(this.comboBox1);
            this.groupAdd.Controls.Add(this.btnEdit);
            this.groupAdd.Controls.Add(this.btnAddNew);
            this.groupAdd.Controls.Add(this.txtCompleted);
            this.groupAdd.Controls.Add(this.txtDescription);
            this.groupAdd.Controls.Add(this.txtName);
            this.groupAdd.Controls.Add(this.txtId);
            this.groupAdd.Controls.Add(this.label3);
            this.groupAdd.Controls.Add(this.label2);
            this.groupAdd.Controls.Add(this.label1);
            this.groupAdd.Controls.Add(this.lblName);
            this.groupAdd.Controls.Add(this.lblId);
            this.groupAdd.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupAdd.Location = new System.Drawing.Point(0, 0);
            this.groupAdd.Name = "groupAdd";
            this.groupAdd.Size = new System.Drawing.Size(200, 380);
            this.groupAdd.TabIndex = 0;
            this.groupAdd.TabStop = false;
            this.groupAdd.Text = "Assigment";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(107, 341);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(80, 23);
            this.btnDelete.TabIndex = 12;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.allUsersViewModelBindingSource, "Email", true));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(15, 256);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(179, 21);
            this.comboBox1.TabIndex = 8;
            // 
            // allUsersViewModelBindingSource
            // 
            this.allUsersViewModelBindingSource.DataSource = typeof(Warehouse_desktop.Models.AllUsersViewModel);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(18, 341);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(80, 23);
            this.btnEdit.TabIndex = 11;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.Location = new System.Drawing.Point(27, 301);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(147, 35);
            this.btnAddNew.TabIndex = 10;
            this.btnAddNew.Text = "Add";
            this.btnAddNew.UseVisualStyleBackColor = true;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // txtCompleted
            // 
            this.txtCompleted.Location = new System.Drawing.Point(15, 196);
            this.txtCompleted.Name = "txtCompleted";
            this.txtCompleted.Size = new System.Drawing.Size(179, 20);
            this.txtCompleted.TabIndex = 8;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(15, 145);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(179, 20);
            this.txtDescription.TabIndex = 7;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(15, 97);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(179, 20);
            this.txtName.TabIndex = 6;
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(15, 46);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(179, 20);
            this.txtId.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 238);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Account Email:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Completed:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Description";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(15, 81);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Name:";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(15, 30);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(19, 13);
            this.lblId.TabIndex = 0;
            this.lblId.Text = "Id:";
            // 
            // btnUcitajPodatke
            // 
            this.btnUcitajPodatke.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUcitajPodatke.Location = new System.Drawing.Point(66, 526);
            this.btnUcitajPodatke.Name = "btnUcitajPodatke";
            this.btnUcitajPodatke.Size = new System.Drawing.Size(75, 23);
            this.btnUcitajPodatke.TabIndex = 5;
            this.btnUcitajPodatke.Text = "Load All";
            this.btnUcitajPodatke.UseVisualStyleBackColor = true;
            this.btnUcitajPodatke.Click += new System.EventHandler(this.btnUcitajPodatke_Click_1);
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 26;
            this.guna2Elipse1.TargetControl = this;
            // 
            // guna2Elipse2
            // 
            this.guna2Elipse2.BorderRadius = 26;
            this.guna2Elipse2.TargetControl = this.pnlFrame;
            // 
            // guna2Elipse3
            // 
            this.guna2Elipse3.BorderRadius = 40;
            // 
            // timerCheckConn
            // 
            this.timerCheckConn.Tick += new System.EventHandler(this.timerCheckConn_Tick);
            // 
            // AssigmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 580);
            this.Controls.Add(this.pnlFrame);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AssigmentForm";
            this.Text = "AssigmentForm";
            this.pnlFrame.ResumeLayout(false);
            this.pnlFrame.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panelDataGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataAssigments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.assigmentBindingSource)).EndInit();
            this.panelOptions.ResumeLayout(false);
            this.panelOptions.PerformLayout();
            this.panelAddNew.ResumeLayout(false);
            this.groupAdd.ResumeLayout(false);
            this.groupAdd.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allUsersViewModelBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlFrame;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse2;
        private System.Windows.Forms.BindingSource assigmentBindingSource;
        private System.Windows.Forms.Panel panelDataGrid;
        private System.Windows.Forms.DataGridView dataAssigments;
        private System.Windows.Forms.Panel panelOptions;
        private System.Windows.Forms.Button btnUcitajPodatke;
        private System.Windows.Forms.Panel panelAddNew;
        private System.Windows.Forms.GroupBox groupAdd;
        private System.Windows.Forms.Button btnAddNew;
        private System.Windows.Forms.TextBox txtCompleted;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Label lblStatusCode;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn completedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountEmailDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel panel1;
        private Guna.UI2.WinForms.Guna2TextBox txtFilter;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.BindingSource allUsersViewModelBindingSource;
        private System.Windows.Forms.Timer timerCheckConn;
    }
}