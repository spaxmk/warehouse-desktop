﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Warehouse_desktop.Helpers;
using Warehouse_desktop.Models;

namespace Warehouse_desktop.Views
{
    public partial class AssigmentForm : Form
    {
        public bool statusConnection = false;

        public AssigmentForm()
        {
            InitializeComponent();
            Timer timerCheckConn = new Timer();
            timerCheckConn.Tick += new EventHandler(timerCheckConn_Tick);
            timerCheckConn.Start();
            if (statusConnection)
            {
                MessageBox.Show("Connection problem.");
            }
            else
            {
                LoadDataAsync();
                LoadAllUsers();
            }
        }

        private async void btnUcitajPodatke_Click_1(object sender, EventArgs e)
        {
            await LoadDataAsync();
        }

        public async Task LoadDataAsync()
        {
            dataAssigments.Update();
            var dataList = await APIHelper.GetAllAssigments();
            dataAssigments.DataSource = dataList;
        }

        private async void btnAddNew_Click(object sender, EventArgs e)
        {
            string Id = txtId.Text.ToString();
            string Name = txtName.Text.ToString();
            string Description = txtDescription.Text.ToString();
            string Completed = txtCompleted.Text.ToString();
            string AccountEmail = comboBox1.SelectedItem.ToString();
            var responce = await APIHelper.PostAssugments(Id, Name, Description, Completed, AccountEmail);
            if (responce != null)
            {
                lblStatusCode.ForeColor = Color.Green;
                lblStatusCode.Text = "Added new";
            }
            else
            {
                lblStatusCode.ForeColor = Color.Red;
                lblStatusCode.Text = "BadRequest";
            }
            LoadDataAsync();
        }

        //Click on row show all value in edit field
        private void dataAssigments_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex != -1)
            {
                DataGridViewRow row = dataAssigments.Rows[e.RowIndex];
                txtId.Text = row.Cells[0].Value.ToString();
                txtName.Text = row.Cells[1].Value.ToString();
                txtDescription.Text = row.Cells[2].Value.ToString();
                txtCompleted.Text = row.Cells[3].Value.ToString();
                comboBox1.SelectedItem = row.Cells[4].Value.ToString();
                lblStatusCode.Text = "";
            }
        }

        private async void btnEdit_Click(object sender, EventArgs e)
        {
            string Id = txtId.Text.ToString();
            string Name = txtName.Text.ToString();
            string Description = txtDescription.Text.ToString();
            string Completed = txtCompleted.Text.ToString();
            string AccountEmail = comboBox1.SelectedItem.ToString();
            var responce = await APIHelper.PutAssigments(Id, Name, Description, Completed, AccountEmail);
            if(responce != null)
            {
                lblStatusCode.ForeColor = Color.Green;
                lblStatusCode.Text = "Updated";
            }
            else
            {
                lblStatusCode.ForeColor = Color.Red;
                lblStatusCode.Text = "BadRequest";
            }

            LoadDataAsync();
        }

        //Delete Assigment
        private async void btnDelete_Click(object sender, EventArgs e)
        {
            int Id = dataAssigments.CurrentCell.RowIndex;
            if (Id == null)
            {
                lblStatusCode.Text = "Please select a field";
            }
            else
            {
                string id = txtId.Text.ToString();
                var responce = await APIHelper.DeleteAssigment(id);
                LoadDataAsync();
                lblStatusCode.Text = "";
            }
        }

        private void txtFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if (txtFilter.Text != "")
                {
                    LoadDataByEmail(txtFilter.Text.ToString());
                    txtFilter.Text = ""; 
                }
                else
                {
                    LoadDataAsync();
                }
            }
        }

        public async Task LoadDataByEmail(string accountEmail)
        {
            dataAssigments.Update();
            var dataList = await APIHelper.GetAllAssigmentsByEmail(accountEmail);
            dataAssigments.DataSource = dataList;
        }

        
        public async Task LoadAllUsers()
        {
            var response = await APIHelper.GetAllUsers();
            foreach(AllUsersViewModel user in response)
            {
                comboBox1.Items.Add(user.Email);
                comboBox1.Enabled = true;
            }
        }

        private async void btnGetAllUsers_ClickAsync(object sender, EventArgs e)
        {
            await LoadAllUsers();
        }

        private void timerCheckConn_Tick(object sender, EventArgs e)
        {
            CheckConn();
        }

        public void CheckConn()
        {
            try
            {
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() != true)
                {
                    if (btnAddNew.Enabled != false || btnEdit.Enabled != false || btnDelete.Enabled != false)
                    {
                        btnAddNew.Enabled = false;
                        btnEdit.Enabled = false;
                        btnDelete.Enabled = false;
                        btnUcitajPodatke.Enabled = false;
                        txtFilter.Enabled = false;

                        statusConnection = false;
                    }
                }
                else
                {
                    if (btnAddNew.Enabled != true || btnEdit.Enabled != true || btnDelete.Enabled != true)
                    {
                        btnAddNew.Enabled = true;
                        btnEdit.Enabled = true;
                        btnDelete.Enabled = true;
                        btnUcitajPodatke.Enabled = true;
                        txtFilter.Enabled = false;

                        statusConnection = true;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw;
            }
        }
    }
}
