﻿using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Warehouse_desktop.Views
{
    public partial class HomeForm : Form
    {
        private Form currentChildForm;

        public HomeForm()
        {
            InitializeComponent();
            ImageSlide.Visible = false;
        }


        private void MoveImageBox(object sender)
        {
            Guna2Button b = (Guna2Button)sender;
            ImageSlide.Location = new Point(b.Location.X + 147, b.Location.Y -  22);
            ImageSlide.SendToBack();
            if (!ImageSlide.Visible)
            {
                ImageSlide.Visible = false;
            }
        }

        private void btnAssigments_Click(object sender, EventArgs e)
        {
            OpenChildForm(new AssigmentForm());
        }

        private void btnWarehouse_CheckedChanged(object sender, EventArgs e)
        {
            MoveImageBox(sender);
        }

        private void btnAssigments_CheckedChanged(object sender, EventArgs e)
        {
            MoveImageBox(sender);
        }

        private void OpenChildForm(Form childForm)
        {
            if (currentChildForm != null)
            {
                currentChildForm.Close();
            }
            currentChildForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            pnlFrame.Controls.Add(childForm);
            pnlFrame.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();

        }

        private void btnWarehouse_Click(object sender, EventArgs e)
        {
            OpenChildForm(new WarehouseForm());
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (currentChildForm != null)
            {
                currentChildForm.Close();
                ImageSlide.Visible = false;
            }
        }

        private void pbQuit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnRegisterNewAccount_Click(object sender, EventArgs e)
        {
            OpenChildForm(new RegisterForm());
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void pnlNav_MouseDown_1(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
    }
}
