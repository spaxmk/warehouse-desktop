﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Warehouse_desktop.Helpers;

namespace Warehouse_desktop.Views
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private async void btnRegister_Click(object sender, EventArgs e)
        {
            string _email;
            string _password;
            string _confirmPassword;
            string _firstName;
            string _lastName; ;
            string _phoneNumber;
            string _adress;
            string _city;
            if (CanRegistration())
            {
                _email = txtEmail.Text.ToString();
                _password = txtPassword.Text.ToString();
                _confirmPassword = txtConfirmPassword.Text.ToString();
                _firstName = txtFirstName.Text.ToString();
                _lastName = txtLastName.Text.ToString();
                _phoneNumber = txtPhoneNumber.Text.ToString();
                _adress = txtAdress.Text.ToString();
                _city = txtCity.Text.ToString();

                var responce = await APIHelper.CreateNewAccount(_email, _password, _confirmPassword, _firstName, _lastName, _phoneNumber, _adress, _city);
                if (responce == null)
                {
                    lblFaildReg.ForeColor = Color.Red;
                    lblFaildReg.Text = "Registration problem. responce is null";
                }
                else
                {
                    txtEmail.Text = "";
                    txtPassword.Text = "";
                    txtConfirmPassword.Text = "";
                    txtFirstName.Text = "";
                    txtLastName.Text = "";
                    txtPhoneNumber.Text = "";
                    txtAdress.Text = "";
                    txtCity.Text = "";
                    lblFaildReg.Text = "";

                    GenerateQrCode(_email, _password);
                    lblScanQRCode.Text = "Scan login QR Code";

                }
            }
            else
            {
                lblFaildReg.ForeColor = Color.Red;
                lblFaildReg.Text = "All fields must be filled";
            }
        }

        public bool CanRegistration()
        {
            
            if (txtEmail.Text == "")
            {
                lblEmailRequired.Text = "Email is required";
                return false;
            }
            else
            {
                lblEmailRequired.Text = "";
            }
            if(txtPassword.Text == "")
            {
                lblPassRequired.Text = "Password is required";
                return false;
            }
            else
            {
                lblPassRequired.Text = "";
            }
            if(txtConfirmPassword.Text == "")
            {
                lblConPrassRequired.Text = "Confired password is required";
                return false;
            }
            else
            {
                lblConfirmPassword.Text = "";
            }
            if (txtPassword.Text.Length < 6)
            {
                lblPassRequired.Text = "The password must be \"Pass123!\" .";
                return false;
            }
            else
            {
                lblPassRequired.Text = "";
            }
            if(txtPassword.Text != txtConfirmPassword.Text)
            {
                lblConPrassRequired.Text = "Password does not match";
                return false;
            }
            else
            {
                lblConPrassRequired.Text = "";
            }
            if (txtFirstName.Text == "")
            {
                lblFirstNameRequired.Text = "First name is required";
                return false;
            }
            else
            {
                lblFirstNameRequired.Text = "";
            }
            if (txtLastName.Text == "")
            {
                lblLastNameRequired.Text = "Last name is required";
                return false;
            }
            else
            {
                lblLastNameRequired.Text = "";
            }
            if (txtPhoneNumber.Text == "")
            {
                lblPhoneNumberRequired.Text = "Phone number is required";
                return false;
            }
            else
            {
                lblPhoneNumberRequired.Text = "";
            }
            if (txtPhoneNumber.Text.Length < 6)
            {
                lblPhoneNumberRequired.Text = "Phone number is incorrect";
                return false;
            }
            else
            {
                lblPhoneNumberRequired.Text = "";
            }
            if (txtAdress.Text == "")
            {
                lblAdresRequired.Text = "Adress is required";
                return false;
            }
            else
            {
                lblAdresRequired.Text = "";
            }
            if(txtCity.Text == "")
            {
                lblCityRequired.Text = "City is required";
                return false;
            }
            else
            {
                lblCityRequired.Text = "";
            }
            return true;
        }

        //Generate QR Core with email & password
        public void GenerateQrCode(string _email, string _password)
        {
            // {"Email":Email, "Password":Password}
            string _json = "{ \"Email\": " + " \" " + _email + "\", \"Password\":\" " + _password + "\"} ";

            QRCoder.QRCodeGenerator qrCode = new QRCoder.QRCodeGenerator();
            var inputData = qrCode.CreateQrCode(_json, QRCoder.QRCodeGenerator.ECCLevel.H);
            var code = new QRCoder.QRCode(inputData);
            pbQRCode.Image = code.GetGraphic(50);

        }

        private void txtPassword_TextChanged_1(object sender, EventArgs e)
        {
            txtPassword.PasswordChar = '*';
        }

        private void txtConfirmPassword_TextChanged(object sender, EventArgs e)
        {
            txtConfirmPassword.PasswordChar = '*';
        }
    }
}
