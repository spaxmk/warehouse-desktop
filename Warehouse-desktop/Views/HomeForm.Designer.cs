﻿namespace Warehouse_desktop.Views
{
    partial class HomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomeForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRegisterNewAccount = new Guna.UI2.WinForms.Guna2Button();
            this.btnAssigments = new Guna.UI2.WinForms.Guna2Button();
            this.btnWarehouse = new Guna.UI2.WinForms.Guna2Button();
            this.ImageSlide = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlNav = new System.Windows.Forms.Panel();
            this.pbQuit = new System.Windows.Forms.PictureBox();
            this.pnlFrame = new System.Windows.Forms.Panel();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.guna2Elipse2 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.pbPanel = new System.Windows.Forms.PictureBox();
            this.lblHomePanel = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageSlide)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlNav.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbQuit)).BeginInit();
            this.pnlFrame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPanel)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnRegisterNewAccount);
            this.panel1.Controls.Add(this.btnAssigments);
            this.panel1.Controls.Add(this.btnWarehouse);
            this.panel1.Controls.Add(this.ImageSlide);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 700);
            this.panel1.TabIndex = 0;
            // 
            // btnRegisterNewAccount
            // 
            this.btnRegisterNewAccount.BackColor = System.Drawing.Color.Transparent;
            this.btnRegisterNewAccount.BorderRadius = 20;
            this.btnRegisterNewAccount.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.btnRegisterNewAccount.CheckedState.Parent = this.btnRegisterNewAccount;
            this.btnRegisterNewAccount.CustomBorderColor = System.Drawing.SystemColors.HighlightText;
            this.btnRegisterNewAccount.CustomImages.Parent = this.btnRegisterNewAccount;
            this.btnRegisterNewAccount.FillColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRegisterNewAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegisterNewAccount.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnRegisterNewAccount.HoverState.Parent = this.btnRegisterNewAccount;
            this.btnRegisterNewAccount.Location = new System.Drawing.Point(16, 640);
            this.btnRegisterNewAccount.Margin = new System.Windows.Forms.Padding(5);
            this.btnRegisterNewAccount.Name = "btnRegisterNewAccount";
            this.btnRegisterNewAccount.ShadowDecoration.Parent = this.btnRegisterNewAccount;
            this.btnRegisterNewAccount.Size = new System.Drawing.Size(173, 46);
            this.btnRegisterNewAccount.TabIndex = 6;
            this.btnRegisterNewAccount.Text = "Register new Account";
            this.btnRegisterNewAccount.UseTransparentBackground = true;
            this.btnRegisterNewAccount.Click += new System.EventHandler(this.btnRegisterNewAccount_Click);
            // 
            // btnAssigments
            // 
            this.btnAssigments.BackColor = System.Drawing.Color.Transparent;
            this.btnAssigments.BorderRadius = 20;
            this.btnAssigments.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.btnAssigments.CheckedState.Parent = this.btnAssigments;
            this.btnAssigments.CustomBorderColor = System.Drawing.SystemColors.HighlightText;
            this.btnAssigments.CustomImages.Parent = this.btnAssigments;
            this.btnAssigments.FillColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAssigments.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAssigments.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnAssigments.HoverState.Parent = this.btnAssigments;
            this.btnAssigments.Location = new System.Drawing.Point(16, 226);
            this.btnAssigments.Margin = new System.Windows.Forms.Padding(5);
            this.btnAssigments.Name = "btnAssigments";
            this.btnAssigments.ShadowDecoration.Parent = this.btnAssigments;
            this.btnAssigments.Size = new System.Drawing.Size(173, 46);
            this.btnAssigments.TabIndex = 3;
            this.btnAssigments.Text = "Assigments";
            this.btnAssigments.UseTransparentBackground = true;
            this.btnAssigments.CheckedChanged += new System.EventHandler(this.btnAssigments_CheckedChanged);
            this.btnAssigments.Click += new System.EventHandler(this.btnAssigments_Click);
            // 
            // btnWarehouse
            // 
            this.btnWarehouse.BackColor = System.Drawing.Color.Transparent;
            this.btnWarehouse.BorderRadius = 20;
            this.btnWarehouse.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.btnWarehouse.CheckedState.Parent = this.btnWarehouse;
            this.btnWarehouse.CustomImages.Parent = this.btnWarehouse;
            this.btnWarehouse.FillColor = System.Drawing.Color.White;
            this.btnWarehouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWarehouse.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnWarehouse.HoverState.Parent = this.btnWarehouse;
            this.btnWarehouse.Location = new System.Drawing.Point(16, 170);
            this.btnWarehouse.Margin = new System.Windows.Forms.Padding(5);
            this.btnWarehouse.Name = "btnWarehouse";
            this.btnWarehouse.ShadowDecoration.Parent = this.btnWarehouse;
            this.btnWarehouse.Size = new System.Drawing.Size(173, 46);
            this.btnWarehouse.TabIndex = 2;
            this.btnWarehouse.Text = "Warehouse";
            this.btnWarehouse.UseTransparentBackground = true;
            this.btnWarehouse.CheckedChanged += new System.EventHandler(this.btnAssigments_CheckedChanged);
            this.btnWarehouse.Click += new System.EventHandler(this.btnWarehouse_Click);
            // 
            // ImageSlide
            // 
            this.ImageSlide.Image = global::Warehouse_desktop.Properties.Resources.selected1;
            this.ImageSlide.Location = new System.Drawing.Point(168, 149);
            this.ImageSlide.Margin = new System.Windows.Forms.Padding(0);
            this.ImageSlide.Name = "ImageSlide";
            this.ImageSlide.Size = new System.Drawing.Size(37, 90);
            this.ImageSlide.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ImageSlide.TabIndex = 5;
            this.ImageSlide.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 146);
            this.panel2.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Warehouse_desktop.Properties.Resources.Warehouse_icon;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(173, 120);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.WaitOnLoad = true;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pnlNav
            // 
            this.pnlNav.Controls.Add(this.pbQuit);
            this.pnlNav.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlNav.Location = new System.Drawing.Point(200, 0);
            this.pnlNav.Name = "pnlNav";
            this.pnlNav.Size = new System.Drawing.Size(824, 81);
            this.pnlNav.TabIndex = 1;
            this.pnlNav.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlNav_MouseDown_1);
            // 
            // pbQuit
            // 
            this.pbQuit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbQuit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbQuit.Image = global::Warehouse_desktop.Properties.Resources.quitIcon;
            this.pbQuit.Location = new System.Drawing.Point(794, 3);
            this.pbQuit.Name = "pbQuit";
            this.pbQuit.Size = new System.Drawing.Size(24, 21);
            this.pbQuit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbQuit.TabIndex = 0;
            this.pbQuit.TabStop = false;
            this.pbQuit.Click += new System.EventHandler(this.pbQuit_Click);
            // 
            // pnlFrame
            // 
            this.pnlFrame.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlFrame.Controls.Add(this.lblHomePanel);
            this.pnlFrame.Controls.Add(this.pbPanel);
            this.pnlFrame.Location = new System.Drawing.Point(200, 81);
            this.pnlFrame.Margin = new System.Windows.Forms.Padding(0, 10, 10, 10);
            this.pnlFrame.Name = "pnlFrame";
            this.pnlFrame.Padding = new System.Windows.Forms.Padding(5);
            this.pnlFrame.Size = new System.Drawing.Size(824, 619);
            this.pnlFrame.TabIndex = 2;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 26;
            this.guna2Elipse1.TargetControl = this;
            // 
            // guna2Elipse2
            // 
            this.guna2Elipse2.BorderRadius = 26;
            this.guna2Elipse2.TargetControl = this.pnlFrame;
            // 
            // pbPanel
            // 
            this.pbPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbPanel.Image = global::Warehouse_desktop.Properties.Resources.Warehouse_icon;
            this.pbPanel.Location = new System.Drawing.Point(261, 130);
            this.pbPanel.Name = "pbPanel";
            this.pbPanel.Size = new System.Drawing.Size(312, 224);
            this.pbPanel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbPanel.TabIndex = 1;
            this.pbPanel.TabStop = false;
            this.pbPanel.WaitOnLoad = true;
            // 
            // lblHomePanel
            // 
            this.lblHomePanel.AutoSize = true;
            this.lblHomePanel.Font = new System.Drawing.Font("Book Antiqua", 26.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomePanel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblHomePanel.Location = new System.Drawing.Point(233, 357);
            this.lblHomePanel.Name = "lblHomePanel";
            this.lblHomePanel.Size = new System.Drawing.Size(373, 43);
            this.lblHomePanel.TabIndex = 2;
            this.lblHomePanel.Text = "Warehouse Desktop App";
            // 
            // HomeForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(1024, 700);
            this.Controls.Add(this.pnlFrame);
            this.Controls.Add(this.pnlNav);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HomeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HomeForm";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ImageSlide)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlNav.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbQuit)).EndInit();
            this.pnlFrame.ResumeLayout(false);
            this.pnlFrame.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPanel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Guna.UI2.WinForms.Guna2Button btnAssigments;
        private Guna.UI2.WinForms.Guna2Button btnWarehouse;
        private System.Windows.Forms.Panel pnlNav;
        private System.Windows.Forms.PictureBox ImageSlide;
        private System.Windows.Forms.Panel pnlFrame;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse2;
        private System.Windows.Forms.PictureBox pbQuit;
        private Guna.UI2.WinForms.Guna2Button btnRegisterNewAccount;
        private System.Windows.Forms.Label lblHomePanel;
        private System.Windows.Forms.PictureBox pbPanel;
    }
}